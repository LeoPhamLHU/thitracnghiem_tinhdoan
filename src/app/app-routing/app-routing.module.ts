import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { DangKyThamGiaComponent } from '../pages/dang-ky-tham-gia/dang-ky-tham-gia.component';
import { ThiTrucTuyenComponent } from '../pages/thi-truc-tuyen/thi-truc-tuyen.component';
import { ThongTinKyThiComponent } from '../pages/thong-tin-ky-thi/thong-tin-ky-thi.component';
import { LamBaiThiComponent, PendingChangesGuard } from '../pages/lam-bai-thi/lam-bai-thi.component';
import { AuthGuardService as AuthGuard } from '../services/auth-guard.service';
import { DangNhapComponent } from '../pages/dang-nhap/dang-nhap.component';


const routes: Routes = [
  // { path: '', redirectTo: '/trang-chu', pathMatch: 'full' },
  // { path: 'trang-chu', component: HomeComponent },
  { path: 'dang-nhap', component: DangNhapComponent },
  { path: 'dang-ky-tham-gia', component: DangKyThamGiaComponent },
  { path: 'thi-truc-tuyen', component: ThiTrucTuyenComponent, canActivate: [AuthGuard] },
  { path: 'thong-tin-ky-thi/:tid', component: ThongTinKyThiComponent, canActivate: [AuthGuard] },
  {
    path: 'lam-bai-thi/:tid', component: LamBaiThiComponent, canActivate: [AuthGuard],
    // canDeactivate: [PendingChangesGuard]
  },
  { path: '**', redirectTo: 'dang-nhap' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }