import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { NgxPageScrollModule } from 'ngx-page-scroll';


import { HomeComponent } from './pages/home/home.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DangKyThamGiaComponent } from './pages/dang-ky-tham-gia/dang-ky-tham-gia.component';
import { PhapLuatService } from './services/phap-luat.service';
import { ThiTrucTuyenComponent } from './pages/thi-truc-tuyen/thi-truc-tuyen.component';
import { ThongTinKyThiComponent } from './pages/thong-tin-ky-thi/thong-tin-ky-thi.component';
import { LamBaiThiComponent, PendingChangesGuard } from './pages/lam-bai-thi/lam-bai-thi.component';
import { LeoQuizComponent } from './components/leo-quiz/leo-quiz.component';
import { LeoQuizInfoComponent } from './components/leo-quiz-info/leo-quiz-info.component';
import { LeoTimerCountdownComponent, FormatTimePipe } from './components/leo-timer-countdown/leo-timer-countdown.component';
import { DangNhapComponent } from './pages/dang-nhap/dang-nhap.component';


registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DangKyThamGiaComponent,
    ThiTrucTuyenComponent,
    ThongTinKyThiComponent,
    LamBaiThiComponent,
    LeoQuizComponent,
    LeoQuizInfoComponent,
    LeoTimerCountdownComponent,
    FormatTimePipe,
    DangNhapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    NgxPageScrollModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    PhapLuatService,
    PendingChangesGuard],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
