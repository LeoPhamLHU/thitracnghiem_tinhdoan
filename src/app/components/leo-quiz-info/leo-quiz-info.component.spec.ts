import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeoQuizInfoComponent } from './leo-quiz-info.component';

describe('LeoQuizInfoComponent', () => {
  let component: LeoQuizInfoComponent;
  let fixture: ComponentFixture<LeoQuizInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeoQuizInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeoQuizInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
