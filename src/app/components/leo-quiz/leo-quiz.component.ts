import { Component, OnInit, Input, Output } from '@angular/core';
import { Quiz } from '../../models/quiz';
import { PhapLuatService } from '../../services/phap-luat.service';
import { Question } from '../../models/question';

import { Router, ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd';
import { QuizConfig } from '../../models/quiz-config';

@Component({
  selector: 'app-leo-quiz',
  templateUrl: './leo-quiz.component.html',
  styleUrls: ['./leo-quiz.component.css']
})
export class LeoQuizComponent implements OnInit {
  @Input() data: Quiz;
  @Input() dataloading: boolean;
  @Output() quizSubmited: boolean = false;
  isSpinning: boolean = false;
  warningMessageText: string = '';
  timeRemain: number = 0;

  config: QuizConfig = {
    'allowBack': false,
    'allowReview': false,
    'autoMove': false,  // if true, it will move to next question automatically when answered.
    'duration': 0,  // indicates the time in which quiz needs to be completed. 0 means unlimited.
    'pageSize': 1,
    'requiredAll': false,  // indicates if you must answer all the questions before submitting.
    'richText': false,
    'shuffleQuestions': false,
    'shuffleOptions': false,
    'showClock': false,
    'showPager': true,
    'theme': 'none'
  };

  pager = {
    index: 0,
    size: 1,
    count: 1
  };


  radioValue = 'A';
  style = {
    display: 'block',
    height: '30px',
    lineHeight: '30px'
  };

  goTo = (index: number) => {
    if (index >= 0 && index < this.pager.count) {
      console.log(`Qua cau: ${index + 1}`)
      this.pager.index = index;
      //this.timeRemain = this.data.questions[this.pager.index].timeVisible;
    }
  }


  get filteredQuestions() {
    return (this.data.questions) ?
      this.data.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
  }



  buildXML() {
    let textXML = '<root>';
    this.data.questions.forEach(ques => {
      textXML += `
        <Question>
          <QuestionNumber>${ques.number}</QuestionNumber>
          <AnswerReply>${+ques.answered | 0}</AnswerReply>
        </Question>`
    });
    textXML += '</root>';
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(textXML, "text/xml");
    let serializer = new XMLSerializer();
    let xmlString = serializer.serializeToString(xmlDoc);
    return xmlString;
  }

  onTick(e) {
    this.data.questions[this.pager.index].timeVisible = e;
    let obj = { id: this.data.id, t: e, q: this.pager.index };
    localStorage.setItem('t', JSON.stringify(obj));
  }

  onFinishedQuestion = (indexQuestion: number) => {
    console.log(`Het gio cau: ${indexQuestion + 1}`)
    if (indexQuestion + 1 == this.pager.count) {
      this.onFinished(1);
    } else
      this.goTo(this.pager.index + 1);
  }

  onFinished(type = 1) {
    if (type === 1) {
      console.log('het gio')
      this.warningMessageText = 'Đã hết giờ làm bài. \n Đang nộp bài, vui lòng không đóng cửa sổ này.'
      this.isSpinning = true;
      this.submitBaiLam();
    } else {
      this.modalService.confirm({
        nzTitle: 'Bạn có chắc muốn nộp bài không?',
        nzContent: '<b style="color: red;">Lứu ý: sau khi nộp bài không thể thay đổi đáp án.</b>',
        nzOkText: 'Có',
        nzOkType: 'danger',
        nzOnOk: () => {
          console.log('nop bai')
          this.warningMessageText = 'Đang nộp bài, vui lòng không đóng cửa sổ này.';
          this.isSpinning = true;
          this.submitBaiLam();
        },
        nzCancelText: 'Không',
        nzOnCancel: () => {
          console.log('khong nop bai')
        }
      });
    }
  }

  submitBaiLam() {
    const id = localStorage.getItem('uid');
    if (this.data && this.data.questions) {
      const xml = this.buildXML();
      // console.log(xml)
      this.quizSubmited = true;
      this._sv.postFinishTest(id, xml).subscribe(res => {
        console.log(res);
        localStorage.removeItem('udata');

        this._router.navigate([`/thi-truc-tuyen`])
      }, null, () => {
        this.isSpinning = false;
      })
    }
  }

  upAnswer(q: Question) {
    const id = localStorage.getItem('uid');
    this._sv.postAnswer({ id, questionNumber: q.number, answerNumber: q.answered }).subscribe(res => {
      if (res[0].ErrCode == 0) {
        this.timeRemain = res[0].RemainTime;
        q.synced = true;
        console.log('Update câu trả lời thành công.', { number: q.number, timeRemain: this.timeRemain })
      } else {
        q.synced = false;
        console.log('Update câu trả lời thất bại.', { error: res[0].ErrMsg })
      }
    }, (err) => {
      q.synced = false;
      console.log('Update câu trả lời thất bại.', { error: err })
    })
  }

  onSelect(q) {
    this.upAnswer(q);
  }


  decodeQuestion(content) {
    return this._sv.b64DecodeUnicode(content)
  }

  constructor(private _sv: PhapLuatService,
    private _router: Router,
    private _route: ActivatedRoute,
    private modalService: NzModalService) { }

  ngOnInit() {

    // this.timeRemain = this.data.questions[this.pager.index].timeVisible;
    // this.timeRemain = this.data.remaintime;


    // if (!this.data.questions) {
    this.pager.count = this.data.questions.length;
    const ifo = JSON.parse(localStorage.getItem('t'));
    if (ifo && ifo.id == this.data.id) {
      this.pager.index = ifo.q;
      this.data.questions[this.pager.index].timeVisible = ifo.t || this.data.timePerQues;
    } else {
      this.pager.index = 0;
      this.data.questions[this.pager.index].timeVisible = this.data.timePerQues;
    }
    // }

    //console.log(this._sv.b64DecodeUnicode('WGUgY8ahIGdp4bubaSAyLTMgYsOhbmggY8OzIMSRxrDhu6NjIGvDqW8gxJHhuql5IG5oYXUgaG/hurdjIHbhuq10IGfDrCBraMOhYyB0csOqbiDEkcaw4budbmcga2jDtG5nPw0K'))
  }

}
