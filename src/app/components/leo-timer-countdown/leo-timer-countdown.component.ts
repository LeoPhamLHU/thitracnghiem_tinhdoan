import { Component, OnInit, Pipe, PipeTransform, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Observable, timer } from 'rxjs'
import { map, take, tap } from 'rxjs/operators'

@Component({
  selector: 'app-leo-timer-countdown',
  templateUrl: './leo-timer-countdown.component.html',
  styleUrls: ['./leo-timer-countdown.component.css']
})
export class LeoTimerCountdownComponent implements OnInit {

  @Input() seconds: string;
  @Output() onFinish: EventEmitter<void> = new EventEmitter();
  @Output() onTick: EventEmitter<number> = new EventEmitter();


  countDown;
  counter: number = 180;
  tick: number = 1000;

  constructor() { }

  ngOnInit() {
    this.counter = parseInt(this.seconds) + 1;
    if (this.counter <= 0) this.onFinish.emit();
    else {
      this.countDown = timer(0, this.tick).pipe(
        take(this.counter),
        map(() => --this.counter),
        tap(s => {
          if (s <= 0)
            this.onFinish.emit()
          else
            this.onTick.emit(s);
        })
      )
    }
  }

  OnDestroy() {
    this.countDown = null;
  }

}


@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {
  transform(value: number): string {
    const minutes: number = Math.floor(value / 60);
    return (`00${minutes}`).slice(-2) + ':' + (`00${Math.floor(value - minutes * 60)}`).slice(-2);
  }
}
