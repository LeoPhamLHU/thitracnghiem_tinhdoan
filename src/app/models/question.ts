import { Option } from "./option";

export class Question {
    id: number;
    name: string;
    questionTypeId: number;
    options: Option[];
    answered: string;
    number: number;
    order: string;
    isChecked: boolean;
    synced: boolean;
    visible: boolean;
    timeVisible: number;

    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.name = data.name;
        this.questionTypeId = data.questionTypeId;
        this.answered = data.answered;
        this.number = data.number || 0;
        this.order = data.order;
        this.options = [];
        this.isChecked = data.isChecked || false;
        this.synced = data.synced || false;
        this.visible = data.visible || false;
        this.timeVisible = data.timeVisible || 10;
        data.options.forEach(o => {
            this.options.push(new Option(o));
        });
    }
}
