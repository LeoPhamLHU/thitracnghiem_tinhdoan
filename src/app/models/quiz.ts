import { QuizConfig } from "./quiz-config";
import { Question } from "./question";

export class Quiz {
    id: number;
    name: string;
    description: string;
    config: QuizConfig;
    questions: Question[];
    totaltime: number;
    remaintime: number;
    isDoing: string; //da bat dau lam bai chua
    soCauHoi: number;
    thoiGian: number;
    timePerQues: number = 10;

    constructor(data: any) {
        if (data) {
            this.id = data.id || 0;
            this.name = data.name || '';
            this.description = data.description || '';
            this.config = new QuizConfig(data.config);
            this.totaltime = data.totaltime || 0;
            this.remaintime = data.remaintime || 0;
            this.questions = [];
            this.isDoing = data.isDoing || false;
            this.soCauHoi = data.soCauHoi || 0;
            this.thoiGian = data.thoiGian || 0;
            this.timePerQues = data.timePerQues || 30;
            data.questions.forEach(q => {
                this.questions.push(new Question(q));
            });
        }
    }
}
