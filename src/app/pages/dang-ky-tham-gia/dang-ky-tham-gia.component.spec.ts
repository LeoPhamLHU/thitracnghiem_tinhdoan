import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DangKyThamGiaComponent } from './dang-ky-tham-gia.component';

describe('DangKyThamGiaComponent', () => {
  let component: DangKyThamGiaComponent;
  let fixture: ComponentFixture<DangKyThamGiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DangKyThamGiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DangKyThamGiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
