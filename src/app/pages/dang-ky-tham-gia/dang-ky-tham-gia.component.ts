import { Component, OnInit } from '@angular/core';
import { PhapLuatService } from '../../services/phap-luat.service';
import { NewUser } from '../../models/new-user';
import { NzMessageService } from 'ng-zorro-antd';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-dang-ky-tham-gia',
  templateUrl: './dang-ky-tham-gia.component.html',
  styleUrls: ['./dang-ky-tham-gia.component.css']
})
export class DangKyThamGiaComponent implements OnInit {
  regForm: FormGroup;
  dsKhoi: any = [];
  dsCap: any = [];
  dsDonVi: any = [];
  dsCoQuan: any = [];
  dsXa: any = [];
  isSubmitting: boolean = false;

  submitForm(): void {
    for (const i in this.regForm.controls) {
      this.regForm.controls[i].markAsDirty();
      this.regForm.controls[i].updateValueAndValidity();
    }
    this.isSubmitting = true;
    let newUser: NewUser = new NewUser(this.regForm.value);
    this._sv.postRegist(newUser).subscribe(res => {
      const data = res[0];
      if (data.ErrCode !== 0) {
        this.message.create('error', data.ErrMsg);
      }
      else {
        this.message.create('success', data.ErrMsg);
      }

    }, (err) => {
      this.message.create('error', err);
    }, () => {
      this.isSubmitting = false;
    })
  }

  getXa(coquanid: number): void {
    this._sv.getXa(coquanid).subscribe(res => {
      this.dsXa = res;
    })
  }

  coquanChange(value: number): void {
    if (!value) return;
    this.dsXa = [];
    this.regForm.get('xa').setValue(null);
    this.getXa(value);
  }

  getCoQuan(donviid: number): void {
    this._sv.getCoQuan(donviid).subscribe(res => {
      this.dsCoQuan = res;
    })
  }

  donviChange(value: number): void {
    if (!value) return;
    this.dsCoQuan = [];
    this.regForm.get('coquan').setValue(null);
    this.dsXa = [];
    this.regForm.get('xa').setValue(null);
    this.getCoQuan(value)
  }

  getDonVi(capid: number): void {
    this._sv.getDonVi(capid).subscribe(res => {
      this.dsDonVi = res;
    })
  }

  capChange(value: number): void {
    if (!value) return;
    this.dsCoQuan = [];
    this.regForm.get('coquan').setValue(null);
    this.dsDonVi = [];
    this.regForm.get('donvi').setValue(null);
    this.dsXa = [];
    this.regForm.get('xa').setValue(null);
    this.getDonVi(value);
  }

  getCap(khoiid: number): void {
    this._sv.getCap(khoiid).subscribe(res => {
      this.dsCap = res;
    })
  }

  khoiChange(value: number): void {
    if (!value) return;
    this.dsCap = [];
    this.regForm.get('cap').setValue(null);
    this.dsCoQuan = [];
    this.regForm.get('coquan').setValue(null);
    this.dsDonVi = [];
    this.regForm.get('donvi').setValue(null);
    this.dsXa = [];
    this.regForm.get('xa').setValue(null);
    this.getCap(value);
  }

  getKhoi(): void {
    this._sv.getKhoi().subscribe(res => {
      this.dsKhoi = res;
    })
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.regForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.regForm.controls.password.value) {
      return { confirm: true, error: true };
    }
  }
  constructor(private fb: FormBuilder, private _sv: PhapLuatService,
    private message: NzMessageService) { }

  ngOnInit() {
    this.getKhoi();

    this.regForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      ngaysinh: [null, [Validators.required]],
      // password: [null, [Validators.required]],
      // checkPassword: [null, [Validators.required, this.confirmationValidator]],
      hoten: [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      sodt: [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]{9,11}$')])],
      cmnd: [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]{9,12}$')])],
      diachi: [null, [Validators.required]],
      khoi: [null, [Validators.required]],
      cap: [null, [Validators.required]],
      donvi: [null, [Validators.required]],
      coquan: [null],
      xa: [null],
      agree: [false, [Validators.required]]
    });
  }


}
