import { Component, OnInit } from '@angular/core';
import { PhapLuatService } from '../../services/phap-luat.service';
import { NzMessageService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-dang-nhap',
  templateUrl: './dang-nhap.component.html',
  styleUrls: ['./dang-nhap.component.css']
})
export class DangNhapComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitting: boolean = false;

  submitForm(): void {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }
    //console.log(this.loginForm.value)
    if (this.loginForm.valid)
      this.isSubmitting = true;
    this._sv.postLogin(this.loginForm.value).subscribe((res: any) => {
      if (res.length > 0) {
        const d: any = res[0];
        if (d.ErrCode == 1)
          this.message.create('error', d.ErrMsg);
        else {
          localStorage.setItem('uid', d.id);
          this._router.navigate([`thi-truc-tuyen`]);
        }
      }
    }, null, () => {
      this.isSubmitting = false;
    })
  }

  constructor(private fb: FormBuilder, private _sv: PhapLuatService
    , private message: NzMessageService,
    private _router: Router) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      // remember: [true]
    });
  }

}
