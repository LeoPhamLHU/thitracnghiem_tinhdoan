import { Component, OnInit, OnDestroy, HostListener, Injectable } from '@angular/core';
import { PhapLuatService } from '../../services/phap-luat.service';
import { ActivatedRoute, Router, CanDeactivate } from '@angular/router';
import { Quiz } from '../../models/quiz';
import { Question } from '../../models/question';
import { Option } from '../../models/option';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-lam-bai-thi',
  templateUrl: './lam-bai-thi.component.html',
  styleUrls: ['./lam-bai-thi.component.css']
})
export class LamBaiThiComponent implements OnInit {

  boDeData: any;
  loading: boolean = true;
  examInfo: any;
  myQuiz: Quiz = new Quiz(null);
  quizSubmited: boolean = false;

  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    // insert logic to check if there are pending changes here;
    // returning true will navigate without confirmation
    // returning false will show a confirm dialog before navigating away
    if (!this.quizSubmited)
      return false;
    return true;
  }

  startExam() {
    let tid = +this.route.snapshot.paramMap.get('tid');
    this._sv.getQuiz(tid).subscribe((res: any) => {
      console.log(res)
      if (res.length == 1) {
        this.router.navigate(['/thi-truc-tuyen']);
      }
      else
        if (res.length > 2) {
          const dataQuiz = this.examInfo;
          const dataQuestion: Question[] = res[0];
          const dataAnswer: Option[] = res[1];

          dataQuestion.forEach(question => {
            let options: Option[];
            options = dataAnswer.filter(x => {
              if (x.questionId === question.id) {
                x.name = x.name;
                x.answerNumber = +x.id;
                x.id = x.questionId + '.' + x.id;
                x.selected = +x.selected === 1 ? true : false;
                return x;
              } else
                return false
            });
            question.questionTypeId = 1;
            question.name = question.name
            const order = question.order.split('');
            let orderedOptions: Option[] = [];
            const ABCD = ['A', 'B', 'C', 'D']
            order.forEach((ord, index) => {
              let opt = options.find(x => x.answerNumber === +ord)
              opt.name = ABCD[index] + '. ' + this._sv.b64DecodeUnicode(opt.name);
              orderedOptions.push(opt)
            });
            question.options = orderedOptions;
          });

          this.myQuiz = new Quiz({ id: dataQuiz.StudentTestID, questions: dataQuestion, remaintime: res[2][0].RemainTime, name: dataQuiz.ExamName, soCauHoi: dataQuiz.SoCauHoi, thoiGian: dataQuiz.ThoiGian })
          this.boDeData = this.myQuiz;
          localStorage.setItem('udata', JSON.stringify(this.myQuiz));
          this.loading = false;
        }
    }, null, () => {
      this.loading = false;
    })
  }

  getExamInfo(callback) {
    this.loading = true;
    let tid = +this.route.snapshot.paramMap.get('tid');
    this._sv.getExamInfo(tid).subscribe((res: any) => {
      if (res.length > 0) {
        this.examInfo = res[0];
        callback();
      }
    }, null, () => {
      // this.loading = false;
    })
  }
  constructor(private _sv: PhapLuatService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.getExamInfo(() => {
      this.startExam();
    });
    // this.boDeData = JSON.parse(localStorage.getItem('udata'));
  }


}



export interface ComponentCanDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}

@Injectable()
export class PendingChangesGuard implements CanDeactivate<ComponentCanDeactivate> {
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> {
    // if there are no pending changes, just allow deactivation; else confirm first
    return component.canDeactivate() ?
      true :
      // NOTE: this warning message will only be shown when navigating elsewhere within your angular app;
      // when navigating away from your angular app, the browser will show a generic warning message
      // see http://stackoverflow.com/a/42207299/7307355
      confirm('Bạn cần hoàn thành bài thi của mình trước khi đóng tab này, nếu không kết quả của bạn có thể không được lưu lại. Bạn có chắc muốn đóng tab này không?');
  }
}