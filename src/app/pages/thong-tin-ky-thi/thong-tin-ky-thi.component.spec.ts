import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongTinKyThiComponent } from './thong-tin-ky-thi.component';

describe('ThongTinKyThiComponent', () => {
  let component: ThongTinKyThiComponent;
  let fixture: ComponentFixture<ThongTinKyThiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongTinKyThiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongTinKyThiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
