import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isAuthenticated(): boolean {

    const uid = localStorage.getItem('uid');
    return (uid && uid != '') ? true : false;
  }
  constructor() { }
}
