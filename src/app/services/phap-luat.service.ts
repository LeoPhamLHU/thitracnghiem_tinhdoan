import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewUser } from '../models/new-user';
@Injectable({
  providedIn: 'root'
})
export class PhapLuatService {
  ApiUrl = 'https://chickenit.club/tinhdoan/etest';


  getQuiz(tid:number){
    return this._http.get(`${this.ApiUrl}/quiz/${tid}`).pipe();
  }

  postLogin(data: any) {
    return this._http.post(`${this.ApiUrl}/login`, {
      Email: data.email,
      MaSo: data.password
    }).pipe()
  }

  postAnswer(data) {
    return this._http.post(`${this.ApiUrl}/Answer`, {
      id: data.id,
      QuestionNumber: data.questionNumber,
      AnswerReply: data.answerNumber || 0
    }).pipe()
  }

  postFinishTest(id, xml) {
    return this._http.post(`${this.ApiUrl}/FinishTest`, { id, QuestionAnswer: xml }).pipe()
  }

  postStartExam(tid: number) {
    return this._http.post(`${this.ApiUrl}/startexam`, { StudentTestID: tid }).pipe()
  }

  getExamInfo(tid: number) {
    return this._http.get(`${this.ApiUrl}/examinfo/${tid}`).pipe();
  }

  postCreateExam(id: string) {
    return this._http.post(`${this.ApiUrl}/createexam`, { uid: id }).pipe()
  }

  getResult(id: string) {
    return this._http.get(`${this.ApiUrl}/result/${id}`).pipe();
  }

  getUserInfo(id: string) {
    return this._http.get(`${this.ApiUrl}/userinfo/${id}`).pipe();
  }

  postRegist(model: NewUser) {
    return this._http.post(`${this.ApiUrl}/register`, model).pipe()
  }

  getXa(coquanid: number) {
    return this._http.get(`${this.ApiUrl}/xa/${coquanid}`).pipe();
  }

  getCoQuan(donviid: number) {
    return this._http.get(`${this.ApiUrl}/coquan/${donviid}`).pipe();
  }

  getDonVi(capid: number) {
    return this._http.get(`${this.ApiUrl}/donvi/${capid}`).pipe();
  }

  getCap(khoiid: number) {
    return this._http.get(`${this.ApiUrl}/cap/${khoiid}`).pipe();
  }
  getKhoi() {
    return this._http.get(`${this.ApiUrl}/khoi`).pipe();
  }

  b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }

  constructor(private _http: HttpClient) { }
}
